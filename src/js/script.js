const openBurgerMenu = document.querySelector(".header__navigation-call-open");
const closeBurgerMenu = document.querySelector(".header__navigation-call-close");
const buttonsBurgerMenu = document.querySelector(".header__navigation-call");
const burgerMenu = document.querySelector(".header__content-burgerMenu");

buttonsBurgerMenu.addEventListener("click", e => {
    openBurgerMenu.classList.toggle("header__navigation-call-open--active");
    closeBurgerMenu.classList.toggle("header__navigation-call-close--active");
    if (document.querySelector(".header__content-burgerMenu").classList.contains("header__content-burgerMenu--active")) {
        burgerMenu.classList.add("fade-out");
        setTimeout(()=> {
            burgerMenu.classList.remove("header__content-burgerMenu--active");
        },1000)
        setTimeout(()=> {
            burgerMenu.classList.remove("fade-out");
        },1500)
    } else {
        burgerMenu.classList.add("fade-in");
        burgerMenu.classList.add("header__content-burgerMenu--active");
        setTimeout(()=> {
            burgerMenu.classList.remove("fade-in");
        },1500)
    }
})
